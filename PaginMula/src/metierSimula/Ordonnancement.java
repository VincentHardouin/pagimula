package metierSimula;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;

import elementSimula.Processus;
import elementSimula.ProcessusFIFO;
import elementSimula.ProcessusNormale;
import elementSimula.ProcessusPriorite;
import elementSimula.ProcessusPrioriteAP;
import elementSimula.ProcessusPrioriteSP;
import elementSimula.ProcessusSrft;


public class Ordonnancement{

	private List<ProcessusNormale> listeProcessusNormale = new ArrayList<ProcessusNormale>();

	private List<ProcessusPriorite> listeProcessusPriorite = new ArrayList<ProcessusPriorite>();
	
	public Ordonnancement() {
		
	}

	public void ajouterProcessus(Processus processusp) {
		if(processusp instanceof ProcessusNormale) {
			this.listeProcessusNormale.add((ProcessusNormale) processusp);
		}

		if(processusp instanceof ProcessusPriorite) {
			this.listeProcessusPriorite.add((ProcessusPriorite) processusp);
		}
	}
	public void clear() {
		listeProcessusNormale.clear();
		listeProcessusPriorite.clear();

	}


	
	public List<ProcessusNormale> ordonnancementSrft(List<ProcessusNormale> list){
		List<ProcessusNormale> listRetour = new ArrayList<ProcessusNormale>();
		
		List<ProcessusSrft> ArrProcessus2 = new ArrayList<ProcessusSrft>(); 
		ProcessusNormale a = new ProcessusNormale("",0,0,0);

		for (ProcessusNormale e: list) {
			ProcessusSrft p1 = new ProcessusSrft(e);
			ArrProcessus2.add(p1);
		}

		List<ProcessusSrft> subarrayinftc = new ArrayList<ProcessusSrft>();

		ProcessusSrft p = new ProcessusSrft(a);
		ProcessusSrft time = new ProcessusSrft(a);


		int t= time.tempsExecutiontotale ( ArrProcessus2);
		int tc= time.minTempsarr(ArrProcessus2);
		System.out.println("|nom\t|ID\t|TA\t|TR\t|");
		while(t>0) {

			for (int i =0; i< ArrProcessus2.size(); i++) {
				if(ArrProcessus2.get(i).getTempsarrive() <=tc && ArrProcessus2.get(i).getTempsexe() >0 ) {
					subarrayinftc.add(ArrProcessus2.get(i));
				}
			}

			if(!subarrayinftc.isEmpty()) {
				Collections.sort(subarrayinftc);
				p=  subarrayinftc.get(0);
				listRetour.add(new ProcessusNormale(p));
				System.out.println(p);
			}

			else { 
				Collections.sort(ArrProcessus2);
				p=  ArrProcessus2.get(0);
				listRetour.add(new ProcessusNormale(p));
				System.out.println(p);
			}

			if (p.getTempsexe() > 0 ) {
				int n = p.getTempsexe();
				p.setTempsexe(n-1);
			}

			int pid = p.getPid();

			if(p.getTempsexe()>0 ) {
				for (int j =0; j<ArrProcessus2.size(); j++) {
					if(ArrProcessus2.get(j).getPid() == pid  ) {
						//bon code si new add ne marche pas
						/*ArrProcessus2.remove(j);
						ArrProcessus2.add(p);*/ 
						ArrProcessus2.set(j, p);

					}
				}
			}
			else{
				for (int j =0; j<ArrProcessus2.size(); j++) {
					if(ArrProcessus2.get(j).getPid() == pid  ) {
						ArrProcessus2.remove(j);
					}
				}
			}


			subarrayinftc.clear();
			tc ++;
			t--;
		}
		
		System.out.println("List retour");
		for(ProcessusNormale p2 : listRetour) {
			System.out.println(p2.toString());
		}
		return listRetour;
	}
	//FIN SRFT

	//DEBUT PP_AP
	
	public List<ProcessusNormale> ordonnancementPP_AP(List<ProcessusPriorite> list){
		
		List<ProcessusNormale> listRetour = new ArrayList<ProcessusNormale>();
		List<ProcessusPrioriteAP> listProcessus = new ArrayList<ProcessusPrioriteAP>(); 
		List<ProcessusPrioriteAP> subarrayinftc = new ArrayList<ProcessusPrioriteAP>();
		
		ProcessusNormale a = new ProcessusNormale("",0, 0, 0);
		ProcessusPriorite b = new ProcessusPriorite(a, 0);

		for (ProcessusPriorite e: list) {
			ProcessusPrioriteAP p1 = new ProcessusPrioriteAP(e);
			listProcessus.add(p1);

		}

		ProcessusPrioriteAP p = new ProcessusPrioriteAP(b);
		ProcessusPrioriteAP time = new ProcessusPrioriteAP(b);

		int t = time.tempsExecutiontotale(listProcessus);
		int tc = time.minTempsarr(listProcessus);

		System.out.println("|nom\t|ID\t|TA\t|TR\t|PR\t|");

		while(t>0) {

			for (int i =0; i< listProcessus.size(); i++) {
				if(listProcessus.get(i).getTempsarrive() <=tc && listProcessus.get(i).getTempsexe() >0 ) {
					subarrayinftc.add(listProcessus.get(i));
				}
			}

			if(!subarrayinftc.isEmpty()) {
				Collections.sort(subarrayinftc);
				p = subarrayinftc.get(0);
				listRetour.add(new ProcessusNormale(p));
				System.out.println(p);
			} else { 
				Collections.sort(listProcessus);
				p = listProcessus.get(0);
				listRetour.add(new ProcessusNormale(p));
				System.out.println(p);
			}

			if (p.getTempsexe() > 0 ) {
				int n = p.getTempsexe();
				p.setTempsexe(n-1);
			}

			int pid = p.getPid();

			if(p.getTempsexe()>0 ) {
				for (int j =0; j<listProcessus.size(); j++) {
					Processus processusList = listProcessus.get(j); 
					if(processusList.getPid() == pid) {
						listProcessus.set(j, p);
					} 
				}
			} else {
				for (int j =0; j<listProcessus.size(); j++) {
					if(listProcessus.get(j).getPid() == pid ) {
						listProcessus.remove(j);
					}
				}
				
			}

			subarrayinftc.clear();
			tc ++;
			t--;
		}
		
		System.out.println("List retour");
		for(ProcessusNormale p2 : listRetour) {
			System.out.println(p2.toString());
		}
		return listRetour;
	}

	private boolean existInList(Processus p, List<Integer> list) {
		for(Integer pid : list) {
			if(pid == p.getPid()) return true;
		}
		return false;
	}
	//FIN PP_AP
	//fifo debut

	public List<ProcessusNormale> ordonnancementFIFO(List<ProcessusNormale> list){
		List<ProcessusNormale> listRetour = new ArrayList<ProcessusNormale>(); 
		List<ProcessusFIFO> ArrProcessus2 = new ArrayList<ProcessusFIFO>(); 
		ProcessusNormale a = new ProcessusNormale("",0,0,0);
		ProcessusFIFO p = new ProcessusFIFO(a);
		for (ProcessusNormale e:  list) {
			ProcessusFIFO p1 = new ProcessusFIFO(e);
			ArrProcessus2.add(p1);

		}

		Collections.sort(ArrProcessus2);
		System.out.println("|nom\t|ID\t|TA\t|TR\t|");
		for(int i=0; i < ArrProcessus2.size(); i++ ) {
			for(int j=0; j < ArrProcessus2.get(i).getTempsexe(); j++ ) {
				p=ArrProcessus2.get(i);
				listRetour.add(new ProcessusNormale(p));
				System.out.println(p);
			}
		}
		System.out.println("List retour");
		for(ProcessusNormale p2 : listRetour) {
			System.out.println(p2.toString());
		}
		return listRetour;
	}
	//fifo fin

	//DEBUT LIFO
	public List<ProcessusNormale> ordonnancementLifo(List<ProcessusNormale> list){
		List<ProcessusNormale> listRetour = new ArrayList<ProcessusNormale>();
		List<ProcessusNormale> listProcessus = new ArrayList<ProcessusNormale>();
		ProcessusNormale p;
		for (Processus pros: list) {
			listProcessus.add( (ProcessusNormale) pros);
		}
		//List<ProcessusNormale> return_ordonnancement = new ArrayList<ProcessusNormale>();
		Collections.sort(listProcessus);
		System.out.println("|nom\t|ID\t|TA\t|TR\t|");
		for(int i=listProcessus.size()-1 ; i >= 0; i--) {
			for(int j=0; j < listProcessus.get(i).getTempsexe(); j++ ) {
				//return_ordonnancement.add(ArrProcessus2.get(i));
				p=listProcessus.get(i);
				listRetour.add(new ProcessusNormale(p));
				System.out.println(p);
			}
		}
		
		System.out.println("List retour");
		for(ProcessusNormale p2 : listRetour) {
			System.out.println(p2.toString());
		}
		
		return listRetour;
		//return return_ordonnancement;
	}
	//FIN LIFO

	//SJF
	public List<ProcessusNormale> ordonnancementSJF(List<ProcessusNormale> list){
		List<ProcessusNormale> listRetour = new ArrayList<ProcessusNormale>();
		List<ProcessusNormale> listProcessus = new ArrayList<ProcessusNormale>();
		ProcessusNormale p =new ProcessusNormale("",0,0,0);
		for (Processus pros:  list) {
			listProcessus.add( (ProcessusNormale) pros);
		}

		//List<ProcessusNormale> return_ordonnancement = new ArrayList<ProcessusNormale>();
		Collections.sort(listProcessus);
		System.out.println("|nom\t|ID\t|TA\t|TR\t|");
		for(int i=0; i < listProcessus.size(); i++ ) {
			for(int j=0; j < listProcessus.get(i).getTempsexe(); j++ ) {
				//return_ordonnancement.add(ArrProcessus2.get(i));
				p=listProcessus.get(i);
				listRetour.add(new ProcessusNormale(p));
				System.out.println(p);
			}
		}
		System.out.println("List retour");
		for(ProcessusNormale pn : listRetour) {
			System.out.println(pn.toString());
		}
		return listRetour; 
		//return return_ordonnancement;
	}

	//FIFO

	//DEBUT NON PREMPTIF
	public List<ProcessusNormale> ordonnancementPP_SP(List<ProcessusPriorite> list){
		List<ProcessusNormale> listRetour = new ArrayList<ProcessusNormale>(); 
		List<ProcessusPrioriteSP> listProcessus = new ArrayList<ProcessusPrioriteSP>(); 
		ProcessusPrioriteSP p ;


		for (ProcessusPriorite e:  list) {
			ProcessusPrioriteSP p1 = new ProcessusPrioriteSP(e);
			listProcessus.add(p1);

		}

		//List<ProcessusPrioriteSP> return_ordonnancement = new ArrayList<ProcessusPrioriteSP>();

		Collections.sort(listProcessus);
		System.out.println("|nom\t|ID\t|TA\t|TR\t|PR\t|");
		for(int i=0; i < listProcessus.size(); i++ ) {
			for(int j=0; j < listProcessus.get(i).getTempsexe(); j++ ) {
				//return_ordonnancement.add(ArrProcessus2.get(i));
				p= listProcessus.get(i);
				listRetour.add(new ProcessusNormale(p));
				System.out.println(p);
			}
		}
		System.out.println("List retour");
		for(ProcessusNormale pn : listRetour) {
			System.out.println(pn.toString());
		}
		return listRetour;
		//return return_ordonnancement;
	}
	//FIN NON PREMPTIF


	//DEBUT ROUND ROBBING
	public List<ProcessusNormale> ordonnancementRR(List<ProcessusNormale> listProcessus,int quantum){
		List<ProcessusNormale> listRetour = new ArrayList<ProcessusNormale>(); 
	
		Collections.sort(listProcessus);
		ProcessusNormale p;
		int nbrQuantum = 1;
		while(!verifieTempsExecutionTotale(listProcessus)){
			System.out.println("Quantum numéro : " + nbrQuantum);
			if(listProcessus.size() > 0) {

				p = listProcessus.get(0);
				for(int i=0; i<quantum; i++) {
					if(p.getTempsexe() != 0) {
						listRetour.add(new ProcessusNormale(p));
						System.out.println(p.toString());
						int n = p.getTempsexe();
						p.setTempsexe(n-1);
						if(p.getTempsexe() == 0) {
							if(listProcessus.size() > 0) {
								listProcessus.remove(0);
								if(listProcessus.size() > 0) {
									p = listProcessus.get(0);
								}
							}
						}
					} else {
						if(listProcessus.size() > 0) {
							listProcessus.remove(0);
							if(listProcessus.size() > 0) {
								p = listProcessus.get(0);
							}
							i--;
						}
					}
				}
				int idProcessusEnCours = p.getPid();
				int idProcessusPremierePosition = 0;
				if(listProcessus.size() > 0) {
					idProcessusPremierePosition = listProcessus.get(0).getPid();
				}

				if(idProcessusEnCours == idProcessusPremierePosition) {
					listProcessus.remove(0);
					if(p.getTempsexe() > 0) {
						listProcessus.add(p);
					}
				}
			}
			nbrQuantum++;
		}
		System.out.println("List retour");
		for(ProcessusNormale pn : listRetour) {
			System.out.println(pn.toString());
		}
		return listRetour;
	}

	private boolean verifieTempsExecutionTotale(List<ProcessusNormale> arrProcessus2) {
		// TODO Auto-generated method stub
		int tet=0;
		for (int k=0; k < arrProcessus2.size(); k++ ) {
			tet += arrProcessus2.get(k).getTempsexe(); 
		}

		if(tet == 0) {
			return true ;
		}
		else {
			return false;
		}

	}
	//FIN ROUND TOBING
}
//< >

