package elementSimula;

public class ProcessusStats {
	private int pid; 
	private int tempsAttente;
	
	public ProcessusStats(int pid) {
		this.pid = pid;
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the pid
	 */
	public int getPid() {
		return pid;
	}

	/**
	 * @return the tempsAttente
	 */
	public int getTempsAttente() {
		return tempsAttente;
	}

	/**
	 * @param tempsAttente the tempsAttente to set
	 */
	public void setTempsAttente(int tempsAttente) {
		this.tempsAttente = tempsAttente;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProcessusStats [pid=" + pid + ", tempsAttente=" + tempsAttente + "]";
	}
	
	

}
