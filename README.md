Projet decrivant les differents algorithmes de gestion des processus par le processeur. Et une autre partie montrant la gestion de la memoire. 

Ce projet a ete realise dans le cadre d'un devoir pour l'universite de Tours L2 Informatique. 

Algorithmes de processus : 
- FIFO 
- LIFO 
- LFU 
- LRU

Algorithmes memoire : 
- FIFO
- LIFO
- SJF
- Round Robin 
- Priorite avec preemption 
- Priorite sans preemption


